import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { addReducer } from './reducers';

const store = createStore(
  combineReducers({addReducer}),
  {},
  applyMiddleware(createLogger())
);
export default store;
