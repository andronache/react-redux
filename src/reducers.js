// const initialState = {
//   posts: [],
//   lastValues: []
// };
// export const addReducer = (state = initialState, action) => {
//   if(action.type === "ADD"){
//     state = {
//       ...state,
//       posts: {
//         title: action.payload.title,
//         content: action.payload.content,
//         id: action.payload.id
//       },
//       lastValues: [...state.lastValues, action.payload]
//     };
//   }
//   return state;
// };
export const addReducer = (state = {lastValues: []}, action) => {
  if(action.type === "ADD"){
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    let fullDate = dd + "." + mm + "." + yyyy;
    //random ID
    let postID = "";
    let pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 5; i++)
      postID += pool.charAt(Math.floor(Math.random() * pool.length));

    state = {
      ...state,
      posts: {
        title: action.payload.title,
        content: action.payload.content,
        id: postID,
        date: fullDate
      },
      lastValues: [...state.lastValues, action.payload]
    };
  }
  return state;
};

export const modifierReducer = (state, action) => {
  switch(action.type) {
    case "DELETE":
      break;
    case "EDIT":
      break;
  }
  return state;
};
