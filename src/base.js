import Rebase from 're-base';
import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyBCr-HUoOBmqD0f6MEAuXcZHKlc-NDLSzM',
  authDomain: 'react-app-95782.firebaseapp.com',
  databaseURL: 'https://react-app-95782.firebaseio.com',
  projectId: 'react-app-95782',
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: '373865704185'
};

const app =  firebase.initializeApp(config);
const base = Rebase.createClass(app.database());

export { base };
