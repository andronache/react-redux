import React from 'react';
import { base } from './base';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import store from './Store';
import { connect } from 'react-redux';
import {
  Home,
  About,
  Add,
  Post,
  Edit
} from './components';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      posts : [],
      lastValues: []
    };
  };
  componentWillMount() {
    this.ref = base.syncState('posts', {
      context: this,
      state: 'posts',
      asArray: true
    });
  }
  componentWillUnmount() {
    base.removeBinding(this.ref);
  };
  deletePost(post) {
    let posts = this.state.posts;
    for(let i = 0; i < posts.length; i++){
      if(posts[i].id === post){
        posts.splice(i, 1);
      }
    }
    this.setState({posts});
  }
  editPost(info) {
    let posts = this.state.posts;
    for(let i = 0; i < posts.length; i++){
      if(posts[i].id === info.id){
        posts[i].title = info.title;
        posts[i].content = info.content;
      }
    }
    this.setState({posts});
  }
  render() {
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} render = {(props) => (
            <Home {...props} edit={this.editPost} del={this.deletePost} posts={this.state.posts}/>
          )} />
          <Route path={"/about"} component={About} />
          <Route path={"/post/:id"} render = {(props) => (
            <Post {...props} edit={this.editPost} del={this.deletePost} />
          )} />
          <Route path={"/edit"} render = {(props) => (
            <Edit {...props} editPost={this.editPost} />
          )} />
          <Route path={"/add"} render = {(props) => (
            <Add {...props} stateClone = {this.state} />
          )} />
        </Switch>
      </Router>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    add: state.addReducer
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    add: (info) => {
      dispatch({
        type: "ADD",
        payload: info
      });
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
